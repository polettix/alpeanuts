#!/bin/sh
md="$(dirname "$(readlink -f "$0")")"
ENTRYPOINT=/usr/sbin/tcpdump \
   LIST='tcpdump' \
   TAGS=alpeanuts:tcpdump   \
   "$md/alpeanuts"
