# Alpeanuts

This is a collection of scripts that leverage [dibs][] to buid small
utility containers, based on [Alpine Linux][].

It should be as easy as running each shell script ending in `.sh`. If
not... please open an [Issue][]!

Copyright 2021 by Flavio Poletti (flavio@polettix.it).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

or look for file `LICENSE` in this project's root directory.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. 

[dibs]: https://github.com/polettix/dibs
[Alpine Linux]: https://www.alpinelinux.org/
[Issue]: https://gitlab.com/polettix/alpeanuts/-/issues
