#!/bin/sh
md="$(dirname "$(readlink -f "$0")")"
ENTRYPOINT=/bin/bash \
   LIST='bash, openssh' \
   TAGS=alpeanuts:bassh   \
   "$md/alpeanuts"
